package com.example.projektarbetespringboot.service;

import com.example.projektarbetespringboot.model.LoginUser;
import com.example.projektarbetespringboot.repo.LoginUserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    LoginUserRepo loginUserRepo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        LoginUser loginUser = loginUserRepo.findUserByUsername(username);
        return new User(loginUser.getUsername(), loginUser.getPassword(), List.of());
    }
}
