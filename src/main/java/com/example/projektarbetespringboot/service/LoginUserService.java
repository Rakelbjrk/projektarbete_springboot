package com.example.projektarbetespringboot.service;

import com.example.projektarbetespringboot.model.DTO.LoginUserDTO;
import com.example.projektarbetespringboot.model.LoginUser;
import com.example.projektarbetespringboot.repo.LoginUserRepo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LoginUserService {

    @Autowired
    LoginUserRepo loginUserRepo;

    public ResponseEntity<List<LoginUser>> getAll() {
        try {
            List<LoginUser> loginUsers = loginUserRepo.findAllUsers();
            return ResponseEntity.ok(loginUsers);
        }catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    public ResponseEntity<LoginUser> findUserById(int id) {
        try {
            return ResponseEntity.ok(loginUserRepo.findUserById(id));
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    public ResponseEntity<LoginUser> addUser(LoginUserDTO loginUserDTO) {
        try {
            return ResponseEntity.ok(loginUserRepo.saveUser(loginUserDTO.toUser()));
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<LoginUser> updateUser(int id, LoginUserDTO loginUserDTO) {
        try {
            LoginUser tempUser = loginUserRepo.findUserById(id);
            loginUserDTO.changePassword();
            loginUserDTO.setUsername(tempUser.getUsername());
            BeanUtils.copyProperties(loginUserDTO, tempUser);
            LoginUser user = loginUserRepo.updateUser(tempUser);
            return ResponseEntity.ok(user);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<HttpStatus> deleteUserById(int id) {
        try {
            loginUserRepo.deleteUserById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
