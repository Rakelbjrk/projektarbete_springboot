package com.example.projektarbetespringboot.service;


import com.example.projektarbetespringboot.model.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.reactive.function.client.WebClientAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Service
public class PostService {

    @Autowired
    WebClient webClient;

    public ResponseEntity<?> getPosts(int id) {
        try {
            List<Post> posts = Arrays.stream(
                            Objects.requireNonNull(webClient
                                    .get()
                                    .uri("https://jsonplaceholder.typicode.com/posts")
                                    .exchangeToMono(clientResponse -> clientResponse.bodyToMono(Post[].class))
                                    .block())
                    )
                    .filter(x -> x.getUserId() == id)
                    .toList();
            return ResponseEntity.ok(posts);
        } catch (Exception e) {
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
