package com.example.projektarbetespringboot.service;

import com.example.projektarbetespringboot.model.DTO.AuthDTO;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;

@Component
public class JwtHelper {

    String nyckel = "Nyckel";
    SecretKey secretKey = new SecretKeySpec(nyckel.getBytes(StandardCharsets.UTF_8),
            "HmacSHA256"
    );


    public String createToken(AuthDTO authDTO) {

        return Jwts.builder()
                .setSubject(authDTO.getUsername())
                .claim("ROLE", "ADMIN")
                .signWith(secretKey)
                .compact();

    }

    public Claims validateToken(String token){
        return Jwts.parserBuilder()
                .setSigningKey(secretKey)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }


}
