package com.example.projektarbetespringboot.model;

import lombok.*;

import java.util.Objects;

@NoArgsConstructor
@Getter @Setter @ToString
public class LoginUser {

    @Setter(AccessLevel.NONE)
    @Getter(AccessLevel.NONE)
    private static int incrementingId = 1;

    private int id;
    private String username;
    private String password;

    public LoginUser(String username, String password) {
        this.id = incrementingId;
        this.username = username;
        this.password = password;
        incrementingId++;
    }

    public LoginUser(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LoginUser)) return false;
        LoginUser loginUser = (LoginUser) o;
        return getId() == loginUser.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}

