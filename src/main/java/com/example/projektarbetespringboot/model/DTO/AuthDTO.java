package com.example.projektarbetespringboot.model.DTO;

public class AuthDTO {

        String username;
        String password;

        public AuthDTO(String username, String password) {
            this.username = username;
            this.password = password;
        }

        public AuthDTO() {
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        @Override
        public String toString() {
            return "AuthDTO{" +
                    "username='" + username + '\'' +
                    ", password='" + password + '\'' +
                    '}';
        }

}
