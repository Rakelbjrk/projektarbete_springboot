package com.example.projektarbetespringboot.model.DTO;

import com.example.projektarbetespringboot.model.LoginUser;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class LoginUserDTO {
    String username;
    String password;
    public LoginUser toUser() {
        return new LoginUser(username, password);
    }
    public LoginUser changePassword() {
        return new LoginUser(password);
    }
}
