package com.example.projektarbetespringboot.repo;

import com.example.projektarbetespringboot.model.LoginUser;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class LoginUserRepo {

    List<LoginUser> loginUserList = new ArrayList<>(List.of(
            new LoginUser("Youssef", "Pass"),
            new LoginUser("Jane", "Doe"),
            new LoginUser("Joe", "Jenkins")
    ));

    public LoginUser findUserByUsername(String username) {
        return loginUserList
                .stream()
                .filter(x -> x.getUsername().equals(username))
                .findFirst()
                .orElseThrow();
    }

    public List<LoginUser> findAllUsers() {
        return loginUserList;
    }

    public LoginUser findUserById(int id) {
        return loginUserList.stream()
                .filter(user -> user.getId() == id)
                .findFirst()
                .orElseThrow();
    }

    public void deleteUserById(int id) {
        loginUserList.removeIf(user -> user.getId() == (id));
    }

    public LoginUser saveUser(LoginUser user) {
        loginUserList.add(user);
        return loginUserList.get(loginUserList.indexOf(user));
    }

    public LoginUser updateUser(LoginUser user) {
        int index = loginUserList.indexOf(user);
        return loginUserList.set(index, user);
    }
}
