package com.example.projektarbetespringboot.controller;

import com.example.projektarbetespringboot.model.DTO.LoginUserDTO;
import com.example.projektarbetespringboot.model.LoginUser;
import com.example.projektarbetespringboot.model.Post;
import com.example.projektarbetespringboot.service.LoginUserService;
import com.example.projektarbetespringboot.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class LoginUserController {

    @Autowired
    LoginUserService loginUserService;

    @Autowired
    PostService postService;

    private int getTempId(Principal principal) {
        return loginUserService.getAll()
                .getBody()
                .stream()
                .filter(x -> x.getUsername().equals(principal.getName()))
                .map(z -> z.getId())
                .findFirst()
                .orElseThrow();
    }

    @GetMapping("/{id}")
    public ResponseEntity<LoginUser> findUserById(
            Principal principal,
            @PathVariable("id") int id
    ) {
        if (getTempId(principal) == id)
            return loginUserService.findUserById(id);

        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    @PostMapping
    public ResponseEntity<LoginUser> addUser(@RequestBody LoginUserDTO loginUserDTO) {
        return loginUserService.addUser(loginUserDTO);
    }

    @PutMapping("/{id}")
    public ResponseEntity<LoginUser> updateUser(
            Principal principal,
            @PathVariable("id") int id,
            @RequestBody LoginUserDTO loginUserDTO
    ) {
        if (getTempId(principal) == id)
            return loginUserService.updateUser(id, loginUserDTO);
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteUserById(
            Principal principal,
            @PathVariable("id") int id
    ) {
        if (getTempId(principal) == id)
            return ResponseEntity.ok(loginUserService.deleteUserById(id));
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    @GetMapping("{id}/posts")
    public ResponseEntity<?> getPosts(
            Principal principal,
            @PathVariable("id") int id
    ) {
        if (getTempId(principal) == id)
            return postService.getPosts(id);
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

}
